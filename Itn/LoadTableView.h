//
//  LoadTableView.h
//  Itn
//
//  Created by Rodrigo Schreiner on 5/15/13.
//  Copyright (c) 2013 Rodrigo Schreiner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellCustomLoadCell.h"
@interface LoadTableView : UITableViewController<UITableViewDataSource>

@end
