//
//  CellCustomLoadCell.m
//  Itn
//
//  Created by Rodrigo Schreiner on 5/15/13.
//  Copyright (c) 2013 Rodrigo Schreiner. All rights reserved.
//

#import "CellCustomLoadCell.h"

@implementation CellCustomLoadCell
@synthesize loadid,customername,locationManager,loadNumber,myBool;

- (IBAction)mySwtich:(id)sender {
	currentlyUsedSwitch = (UISwitch*)sender;

	self.locationManager= [[CLLocationManager alloc]init];
	locationManager.delegate = self;
	locationManager.desiredAccuracy=kCLLocationAccuracyBest;
	locationManager.distanceFilter= 5.0f;
	if ([sender isOn]) {
		NSLog ( @" currently used switch == %@", currentlyUsedSwitch);
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ITN" message:@"Do you really want to track this load?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
		[alert addButtonWithTitle:@"Yes"];
		[alert show];
	}
	else{
		
		[self gpsOff];
	}
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
		[self gpsOn];
    }
	else{
		//NSLog ( @" currently used switch == %@", currentlyUsedSwitch);
		[currentlyUsedSwitch setOn:NO animated:YES];
	}
}

- (void) gpsOn
{
	[locationManager startUpdatingLocation];
}

- (void) gpsOff
{
	[locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    CLLocationCoordinate2D currentCoordinates = newLocation.coordinate;
    
	//NSLog(@"Entered new Location with the coordinates Latitude: %f Longitude: %f", currentCoordinates.latitude, currentCoordinates.longitude);
	
	float latitude = currentCoordinates.latitude;
	float longitude = currentCoordinates.longitude;
	NSString *urlString = [NSString stringWithFormat:@"http://www.goemobile.com/mobile/loader.php?loadid=%@&loadnumber=%@&longitude=%f&latitude=%f",loadid,loadNumber.text,latitude,longitude];
	//NSLog(@"%@",urlString);
	//[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
	
	//NSString *stringResult = [[[NSString alloc] initWithData:urlString encoding:NSUTF8StringEncoding]autorelease];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"Unable to start location manager. Error:%@", [error description]);
}

@end
