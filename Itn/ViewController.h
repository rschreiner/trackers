//
//  ViewController.h
//  Itn
//
//  Created by Rodrigo Schreiner on 5/14/13.
//  Copyright (c) 2013 Rodrigo Schreiner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RodrigoTableViewController.h"

@interface ViewController : UIViewController<UITextFieldDelegate>{
	RodrigoTableViewController * drigo;
}
@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) NSString *uuidtoken;
- (IBAction)login:(id)sender;
- (IBAction)textFieldReturn:(id)sender;
- (IBAction)backgroundTouched:(id)sender;
@property (nonatomic, strong) NSDictionary *loginJson;
@end
