//
//  ViewController.m
//  Itn
//
//  Created by Rodrigo Schreiner on 5/14/13.
//  Copyright (c) 2013 Rodrigo Schreiner. All rights reserved.
//

#import "ViewController.h"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
@interface ViewController ()

@end

@implementation ViewController
@synthesize username,password,loginJson,uuidtoken;
- (void)viewDidLoad
{
    [super viewDidLoad];
	self.password.delegate = self;
	self.username.delegate = self;
//	self.uuidtoken= @"874BA1A0-C29F-C268-5E79951BBC80A5B0";
}

- (void)viewWillAppear:(BOOL)animated {
	[self.navigationController setNavigationBarHidden:TRUE];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
- (IBAction)login:(id)sender {
	RodrigoTableViewController *test = [[RodrigoTableViewController alloc] initWithNibName:@"RodrigoTableViewController" bundle:nil];
	//[self.view addSubview:test.view];
    
    self.view.window.rootViewController = test;
}
*/
- (IBAction)login:(id)sender {
	 [username resignFirstResponder];
	 [password resignFirstResponder];
	 if([username.text length] == 0 || [password.text length] == 0){
		 UIAlertView *messageError = [[UIAlertView alloc] initWithTitle:@"ITN"
																message:@"Username and Password are required."
															   delegate:self
													  cancelButtonTitle:@"OK"
													  otherButtonTitles:nil];
		 [messageError show];
	 }
	 else{
		 NSString *movieid = @"550";
		//NSString *urlString = [NSString stringWithFormat:@"https://test.itradenetwork.com/secure/login/loginproxy.cfm?uname=%@&pword=%@",username.text,password.text];
		// NSLog(@"url = %@",urlString);
		 NSString *urlString = [NSString stringWithFormat:@"http://www.goemobile.com/mobile/json.php"];
		 dispatch_async(kBgQueue, ^{
			 NSURL *url = [NSURL URLWithString:urlString];
			 NSData* data = [NSData dataWithContentsOfURL:url];
			// NSLog(@"Dat = %@",data);
			 [self performSelectorOnMainThread:@selector(fetchLogin:)withObject:data waitUntilDone:NO];
		 });
	 }
}

- (void)fetchLogin:(NSData *)responseData {
	
	NSError* error;
    loginJson = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
	uuidtoken = [loginJson objectForKey:@"success"];
	RodrigoTableViewController *test = [[RodrigoTableViewController alloc] initWithNibName:@"RodrigoTableViewController" bundle:nil];
	//self.view.window.rootViewController = test;
	[self.navigationController pushViewController:test animated:TRUE];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	return [textField resignFirstResponder];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [username resignFirstResponder];
	[password resignFirstResponder];
}
@end
