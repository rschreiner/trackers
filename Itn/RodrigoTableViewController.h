//
//  RodrigoTableViewController.h
//  Itn
//
//  Created by Rodrigo Schreiner on 5/15/13.
//  Copyright (c) 2013 Rodrigo Schreiner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellCustomLoadCell.h"
@interface RodrigoTableViewController : UITableViewController{
	NSArray *news;
	NSMutableData *data;
    IBOutlet UISwitch *mySwitch;
	IBOutlet UITableView *mainTableView;
    NSArray *results;
}
@end
