//
//  RodrigoTableViewController.m
//  Itn
//
//  Created by Rodrigo Schreiner on 5/15/13.
//  Copyright (c) 2013 Rodrigo Schreiner. All rights reserved.
//

#import "RodrigoTableViewController.h"
@interface RodrigoTableViewController ()

@end
@implementation RodrigoTableViewController

- (void)viewDidLoad {
	
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:@"CellCustomLoadCell" bundle:nil] forCellReuseIdentifier:@"CellCustomLoadCell"];
	[UIApplication sharedApplication].networkActivityIndicatorVisible=YES;

	 NSURL *urlString = [NSURL URLWithString:@"http://api.themoviedb.org/3/search/movie?page=1&api_key=34eb86f3b94de2676e8d3007b5ce1993&query=rambo"];
	 NSURLRequest *request = [NSURLRequest requestWithURL:urlString];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void)viewWillAppear:(BOOL)animated {
	[self.navigationController setNavigationBarHidden:FALSE];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	data = [[NSMutableData alloc] init];
	
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData
{
	[data appendData:theData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
	
	//news = [NSJSONSerialization JSONObjectWithData:data options:nil error:nil];
	NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:nil error:nil];
	
	results = [json objectForKey:@"results"];
	[mainTableView reloadData];
	
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	UIAlertView *errorView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"The download could not finish" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
	[errorView show];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
	
}

-(int)numberOfSectionsInTableView:(UITableView *)tableView{
	return 1;
}

-(int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return results.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CellCustomLoadCell";
    CellCustomLoadCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
	NSDictionary *appsdict = [results objectAtIndex:indexPath.row];
	NSString *id = [appsdict objectForKey:@"id"];
	NSString *id1 = [appsdict objectForKey:@"title"];
	NSString *load = [NSString stringWithFormat:@"%@%@",id,id1];
	cell.loadNumber.text=load;
	cell.loadid=id;
	//UISwitch *switchview = [[UISwitch alloc] initWithFrame:CGRectZero];
   // cell.accessoryView = switchview;
	
	//[switchview addTarget:self action:@selector(mySwitchValueChanged:) forControlEvents:UIControlEventValueChanged];
	cell.customername=[appsdict objectForKey:@"adult"];
    return cell;
}
/*
- (void)startTracking:(UISwitch*)switchView{
	
	if ([switchView isOn]) {
		[switchView setOn:NO animated:YES];
	} else {
		[switchView setOn:YES animated:YES];
	}
	
}
 */

@end
